function myRequest(options = {}) {
	return new Promise((resolve, reject) => {
		uni.request({
			url: options.url,
			method: options.method || 'GET',
			data: options.data || {},
			header: {
				// #ifdef H5
				xip: localStorage.getItem('Ip'),
				Authorization: localStorage.getItem('token')
				// #endif
				// #ifndef H5
				xip: uni.getStorageSync('ip'),
				Authorization: uni.getStorageSync('token')
				// #endif
			},
			success: (res) => {
				resolve(res)
			},
			fail: (err) => {
				uni.showToast({
					title: '请求接口失败',
					duration: 1000
				})
				reject(err)
			}
		})
	})
}

export default myRequest
