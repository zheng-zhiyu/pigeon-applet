import myRequest from '@/services/request.js'
const baseUrl = 'http://120.77.156.205:9801'

export function getUserInfo() {
	return new Promise((resolve, reject) => {
		let userInfo = uni.getStorageSync('userInfo')
		if(userInfo) {
			resolve(userInfo)
		} else {
			uni.getUserProfile({
				desc: "获取头像和昵称",
				lang: "zh_CN",
				success(res) {
					// console.log(res)
					let userInfo = res.userInfo
					userInfo.cloudID = res.cloudID
					uni.setStorage({
						key: 'userInfo',
						data: userInfo
					})
					resolve(userInfo)
				},fail(err) {
					reject(err)
				}
			})
		}
	})
}

export function getCaptcha() {
	return myRequest({
		url: `${baseUrl}/authority/captcha`
	})
}

export function login(data) {
	return myRequest({
		url: `${baseUrl}/authority/user/login`,
		method: 'put',
		data
	})
}

export function loginByWx(data) {
	return myRequest({
		url: `${baseUrl}/authority/user/wechat/login`,
		method: 'post',
		data
	})
}

export function bindWx(data) {
	return myRequest({
		url: `${baseUrl}/authority/user/wechat/bind`,
		method: 'post',
		data
	})
}